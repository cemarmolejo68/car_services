import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:google_nav_bar/google_nav_bar.dart';

import 'login_screen.dart';


class Homepage extends StatefulWidget {
  const Homepage({super.key});

  @override
  State<Homepage> createState() => _HomepageState();
}

enum Screen {
  home,
  favorite,
  cart,
  profile,
}

class _HomepageState extends State<Homepage> {

  Screen _currentScreen = Screen.home;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFFBF3E4),
      

     body: _buildScreen(_currentScreen),
      bottomNavigationBar: GNav(
        selectedIndex: _currentScreen.index,
        onTabChange: (index) {
          setState(() {
            _currentScreen = Screen.values[index];
          });
        },
        gap: 15,
        backgroundColor: const Color.fromARGB(255, 92, 92, 92),
        tabBackgroundColor: const Color.fromARGB(255, 153, 153, 153),
        padding: const EdgeInsets.all(16),
        tabs: const[
          GButton(
            icon: Icons.car_repair,
            text: 'Car Parts',
          ),
          GButton(
            icon: Icons.car_crash,
            text: 'Servicing',
          ),
          GButton(
            icon: Icons.settings,
            text: 'Settings',
          ),
          GButton(
            icon: Icons.person,
            text: 'Profile',
          ),
        ],
      ),
    );
  }

  Widget _buildScreen(Screen screen) {
    switch (screen) {
      case Screen.home:
        return const HomeScreen();
      case Screen.favorite:
        return FavoriteScreen();
      case Screen.cart:
        return const CartScreen();
      case Screen.profile:
        return const ProfileScreen();
      default:
        return Container();
    }
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              const SizedBox(height: 20,),

              Column(
                children: const [
                  Text(
                    'Car Parts',
                    textAlign: TextAlign.center,
                      style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: Colors.black
                    ),
                  ),
    
                ],
              ),

      const SizedBox(height: 30,),
              Row(
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(40),
                          child: const Image(
                            height: 120,
                            image: AssetImage('assets/3.png'),
                          ),
                        ),
                        const Text('Exhaust', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                      ],
                    ),
                  ),

                  Expanded(
                    child: Column(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(40),
                          child: const Image(
                            height: 120,
                            image: AssetImage('assets/4.png'),
                          ),
                        ),
                        const Text('Brakes', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                      ],
                    ),
                  ),
                ],
              ),

              const SizedBox(height: 30,),
              Row(
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(40),
                          child: const Image(
                            height: 120,
                            image: AssetImage('assets/1.png'),
                          ),
                        ),
                        const Text('Engines', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, ),
                    ),
                      ],
                    ),
                  ),

                  Expanded(
                    child: Column(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(40),
                          child: const Image(
                            height: 120,
                            image: AssetImage('assets/5.png'),
                          ),
                        ),
                        const Text('Suspensions', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                      ],
                    ),
                  ),
                ],
              ),

              const SizedBox(height: 30,),
              Row(
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(40),
                          child: const Image(
                            height: 120,
                            image: AssetImage('assets/2.png'),
                          ),
                        ),
                        const Text('Tires/Wheels', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                      ],
                    ),
                  ),

                  Expanded(
                    child: Column(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(40),
                          child: const Image(
                            height: 120,
                            image: AssetImage('assets/6.png'),
                          ),
                        ),
                        const Text('Drivetrains', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                      ],
                    ),
                  ),
                ],
              ),
              
            ],
          ),
        ),
       ),
    );
  }
}

class FavoriteScreen extends StatelessWidget {
  final items = ['Exhausts', 'Brakes', 'Engines', 'Suspensions', 'Tires/Wheels', 'Drivetrains'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        
        child: Center(
          child: Column(
            children:  [
              const SizedBox(height: 20,),
              const Text(
                'Servicing',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 30
                ),
                ),

              const SizedBox(height: 100,),
              const Text(
                'Reserve slot for servicing',
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 25
                ),
              ),

            const SizedBox(height:10,),
            Row(
              children: [
                Expanded(
                  child: TextButton(
                    onPressed: (){
                      DatePicker.showDatePicker(context,
                        showTitleActions: true,
                        minTime: DateTime(2000, 12, 31),
                        maxTime: DateTime(2050, 12, 31), onChanged: (date) {
                      print('change $date');
                    }, onConfirm: (date) {
                      print('confirm $date');
                    }, currentTime: DateTime.now(), locale: LocaleType.en);
                    }, 
                  child: const Text("Exhausts", style: TextStyle(
                    fontSize: 17,
                    color: Colors.red,
                    fontWeight: FontWeight.bold
                    
                  ),
                  ),
                  ),
                ),

                Expanded(
                  child: TextButton(
                    onPressed: (){
                      DatePicker.showDatePicker(context,
                        showTitleActions: true,
                        minTime: DateTime(2000, 12, 31),
                        maxTime: DateTime(2050, 12, 31), onChanged: (date) {
                      print('change $date');
                    }, onConfirm: (date) {
                      print('confirm $date');
                    }, currentTime: DateTime.now(), locale: LocaleType.en);
                    }, 
                  child: const Text("Brakes", style: TextStyle(
                    fontSize: 17,
                    color: Colors.red,
                    fontWeight: FontWeight.bold
                    
                  ),
                  ),
                  ),
                ),

                Expanded(
                  child: TextButton(
                    onPressed: (){
                      DatePicker.showDatePicker(context,
                        showTitleActions: true,
                        minTime: DateTime(2000, 12, 31),
                        maxTime: DateTime(2050, 12, 31), onChanged: (date) {
                      print('change $date');
                    }, onConfirm: (date) {
                      print('confirm $date');
                    }, currentTime: DateTime.now(), locale: LocaleType.en);
                    }, 
                  child: const Text("Engines", style: TextStyle(
                    fontSize: 17,
                    color: Colors.red,
                    fontWeight: FontWeight.bold
                    
                  ),
                  ),
                  ),
                ),
              ],
            ),

            Row(
              children: [
                Expanded(
                  child: TextButton(
                    onPressed: (){
                      DatePicker.showDatePicker(context,
                        showTitleActions: true,
                        minTime: DateTime(2000, 12, 31),
                        maxTime: DateTime(2050, 12, 31), onChanged: (date) {
                      print('change $date');
                    }, onConfirm: (date) {
                      print('confirm $date');
                    }, currentTime: DateTime.now(), locale: LocaleType.en);
                    }, 
                  child: const Text("Suspensions", style: TextStyle(
                    fontSize: 17,
                    color: Colors.red,
                    fontWeight: FontWeight.bold
                    
                  ),
                  ),
                  ),
                ),

                Expanded(
                  child: TextButton(
                    onPressed: (){
                      DatePicker.showDatePicker(context,
                        showTitleActions: true,
                        minTime: DateTime(2000, 12, 31),
                        maxTime: DateTime(2050, 12, 31), onChanged: (date) {
                      print('change $date');
                    }, onConfirm: (date) {
                      print('confirm $date');
                    }, currentTime: DateTime.now(), locale: LocaleType.en);
                    }, 
                  child: const Text("Tires/Wheels", style: TextStyle(
                    fontSize: 17,
                    color: Colors.red,
                    fontWeight: FontWeight.bold
                    
                  ),
                  ),
                  ),
                ),

                Expanded(
                  child: TextButton(
                    onPressed: (){
                      DatePicker.showDatePicker(context,
                        showTitleActions: true,
                        minTime: DateTime(2000, 12, 31),
                        maxTime: DateTime(2050, 12, 31), onChanged: (date) {
                      print('change $date');
                    }, onConfirm: (date) {
                      print('confirm $date');
                    }, currentTime: DateTime.now(), locale: LocaleType.en);
                    }, 
                  child: const Text("Drivetrains", style: TextStyle(
                    fontSize: 17,
                    color: Colors.red,
                    fontWeight: FontWeight.bold
                    
                  ),
                  ),
                  ),
                ),
              ],
            ),

              const SizedBox(height: 70,),
              const Text(
                'View available reservation slots',
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 25
                ),
              ),

               const SizedBox(height:10,),
            Row(
              children: [
                Expanded(
                  child: TextButton(
                    onPressed: (){

                    }, 
                  child: const Text("Exhausts", style: TextStyle(
                    fontSize: 17,
                    color: Colors.grey,
                    fontWeight: FontWeight.bold
                    
                  ),
                  ),
                  ),
                ),

                Expanded(
                  child: TextButton(
                    onPressed: (){

                    }, 
                  child: const Text("Brakes", style: TextStyle(
                    fontSize: 17,
                    color: Colors.grey,
                    fontWeight: FontWeight.bold
                    
                  ),
                  ),
                  ),
                ),

                Expanded(
                  child: TextButton(
                    onPressed: (){

                    }, 
                  child: const Text("Engines", style: TextStyle(
                    fontSize: 17,
                    color: Colors.grey,
                    fontWeight: FontWeight.bold
                    
                  ),
                  ),
                  ),
                ),
              ],
            ),

            Row(
              children: [
                Expanded(
                  child: TextButton(
                    onPressed: (){

                    }, 
                  child: const Text("Suspensions", style: TextStyle(
                    fontSize: 17,
                    color: Colors.grey,
                    fontWeight: FontWeight.bold
                    
                  ),
                  ),
                  ),
                ),

                Expanded(
                  child: TextButton(
                    onPressed: (){

                    }, 
                  child: const Text("Tires/Wheels", style: TextStyle(
                    fontSize: 17,
                    color: Colors.grey,
                    fontWeight: FontWeight.bold
                    
                  ),
                  ),
                  ),
                ),

                Expanded(
                  child: TextButton(
                    onPressed: (){

                    }, 
                  child: const Text("Drivetrains", style: TextStyle(
                    fontSize: 17,
                    color: Colors.grey,
                    fontWeight: FontWeight.bold
                    
                  ),
                  ),
                  ),
                ),
              ],
            ),
                
            ],
          ),
        ),
       ),
    );
  }
}

class CartScreen extends StatelessWidget {
  const CartScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       body: SafeArea(
        child: Center(
          child: Column(
            children:  [
              const SizedBox(height: 20,),
              const Text(
                'Settings',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 30
                ),
                ),
                 
              const SizedBox(height: 150,),  
              ElevatedButton(
              onPressed: () {
              },
              style: ElevatedButton.styleFrom(
                minimumSize: const Size(200, 20),
                elevation: 20,
                backgroundColor: const Color.fromARGB(255, 0, 0, 0),
                shadowColor: Colors.black,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5), // <-- Radius
                ),
              ),
              child: const Padding(
                padding: EdgeInsets.all(10.0),
                child: Text('Give Feedback', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
              ),
                        ),

            const SizedBox(height: 10,),  
              ElevatedButton(
              onPressed: () {
              },
              style: ElevatedButton.styleFrom(
                minimumSize: const Size(200, 20),
                elevation: 20,
                backgroundColor: const Color.fromARGB(255, 0, 0, 0),
                shadowColor: Colors.black,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5), // <-- Radius
                ),
              ),
              child: const Padding(
                padding: EdgeInsets.all(10.0),
                child: Text('Rate us', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
              ),
            ),

            const SizedBox(height: 10,), 
             
              ElevatedButton(
              onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => const Login()));
              },
              style: ElevatedButton.styleFrom(
                minimumSize: const Size(200, 20),
                elevation: 20,
                backgroundColor: const Color.fromARGB(255, 0, 0, 0),
                shadowColor: Colors.black,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5), // <-- Radius
                ),
              ),
              child: const Padding(
                padding: EdgeInsets.all(10.0),
                child: Text('Log out', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
              ),
            ),

            ],
          ),
        ),
       ),
    );
  }
}

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
            const SizedBox(height: 20,),
            const Text(
                'Profile',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 30
                ),
                ),

            const SizedBox(height: 20,),
            const SizedBox(
              height: 200,
              width: 200,
              child: CircleAvatar(
                    radius: 100,
                    backgroundImage: AssetImage('assets/carpic.jpg'),
                ),
            ),

            const SizedBox(height: 10,),
            const Text(
                 'Koenigseg G. Jesko',
                  style: TextStyle(
                   fontWeight: FontWeight.bold,
                   fontSize: 22
                ),
              ),

              const SizedBox(height: 120,), 
              ElevatedButton(
              onPressed: () {

              },
              style: ElevatedButton.styleFrom(
                minimumSize: const Size(250, 20),
                elevation: 20,
                backgroundColor: const Color.fromARGB(255, 0, 0, 0),
                shadowColor: Colors.black,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5), // <-- Radius
                ),
              ),
              child: const Padding(
                padding: EdgeInsets.all(10.0),
                child: Text('Edit Profile', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
              ),
            ),

            const SizedBox(height: 10,), 
              ElevatedButton(
              onPressed: () {

              },
              style: ElevatedButton.styleFrom(
                minimumSize: const Size(250, 20),
                elevation: 20,
                backgroundColor: const Color.fromARGB(255, 0, 0, 0),
                shadowColor: Colors.black,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5), // <-- Radius
                ),
              ),
              child: const Padding(
                padding: EdgeInsets.all(10.0),
                child: Text('Change Profile Photo', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
              ),
            ),

            const SizedBox(height: 10,), 
              ElevatedButton(
              onPressed: () {

              },
              style: ElevatedButton.styleFrom(
                minimumSize: const Size(250, 20),
                elevation: 20,
                backgroundColor: const Color.fromARGB(255, 0, 0, 0),
                shadowColor: Colors.black,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5), // <-- Radius
                ),
              ),
              child: const Padding(
                padding: EdgeInsets.all(10.0),
                child: Text('Change Password', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
              ),
            ),

            ],
          ),
        ),
       ),
    );
  }
}